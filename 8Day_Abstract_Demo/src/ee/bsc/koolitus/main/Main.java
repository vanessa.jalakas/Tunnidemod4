package ee.bsc.koolitus.main;

import ee.bsc.koolitus.inheritance.Gender;
import ee.bsc.koolitus.inheritance.Human;
import ee.bsc.koolitus.inheritance.Student;
import ee.bsc.koolitus.inheritance.Teacher;

public class Main {

	public static void main(String[] args) {
		Human inimene = new Human("Vanessa", "Jalakas");
		inimene.setFirstName("Vanessa");
		inimene.setLastName("Jalakas").setGender(Gender.FEMALE);

		Student pupil = new Student();
		// pupil.setFirstName("Mati");
		pupil.setLastName("Kala");
		pupil.setGender(Gender.MALE);
		pupil.setGrade("first");

		System.out.println(inimene);
		System.out.println(pupil);

		System.out.println("\n" + "Humans list: " + Human.humanList);
		Human.humanList.remove(inimene);

		System.out.println("Human list after remove: " + Human.humanList);
		
		//Casting
		Human studentMari = new Student();
		((Student)studentMari).getGrade();
		Human humanMati = new Human("Mati", "Kool");
		
		
		
//		if (humanMati instanceof Student) { //instanceof ütleb, et kas humanMati vastab Student classi kirjeldusele.
//			System.out.println("Mari on õpilane");
//		}else {
//			System.out.println("Mari ei ole õpilane"); //Ei vasta, kuna -> new Human("M....
//		}
//		
//		
//		Human test = null; 
//		
//		if (test instanceof Human) { //instanceof ütleb, et kas humanMati vastab Student classi kirjeldusele.
//			System.out.println("Test on inimene");
//		}else {
//			System.out.println("Test ei ole inimene"); //Annab vastuse, et Test ei ole õpilane. 
//		}
		
		Teacher teacherList = new Teacher();
		teacherList.addItemToTeacher("pencil", "apple");
		
		System.out.println(teacherList);

	}

}
