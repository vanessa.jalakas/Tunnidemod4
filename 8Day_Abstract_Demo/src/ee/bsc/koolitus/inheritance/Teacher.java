package ee.bsc.koolitus.inheritance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class Teacher extends Human {

	private final ArrayList<String> schoolItems = new ArrayList<>();

	public Teacher() {
		super("Kati", "Õpetaja");
	}

	public Teacher(String firstName, String lastname) {
		super(firstName, lastname);
	}

	public void addItemToTeacher(String item) {
		this.schoolItems.add(item);
	}

	public void addItemToTeacher(String... items) {
		this.schoolItems.addAll(Arrays.asList(items));
	}
	
	public ArrayList<String> getSchoolItems() {
		return (ArrayList<String>) Collections.unmodifiableList(schoolItems);
	}

	@Override
	public String toString() {
		String itemsList = "";
		for (int i = 0; i < schoolItems.size(); i++) {
			itemsList += " " + schoolItems.get(i);

		}
		return "Teacher items: " + itemsList;
	}

}
