package ee.bsc.koolitus.inheritance;

import java.util.ArrayList;
import java.util.List;

public class Human {
	protected String firstName;
	String lastName; // default
	private Gender gender;

	public final static List<Human> humanList = new ArrayList<Human>();

	public Human(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
		humanList.add(this);

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Human setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public Gender getGender() {
		return gender;
	}

	public Human setGender(Gender gender) {
		this.gender = gender;
		return this;
	}

	@Override
	public String toString() {
		return "Human[firstname=" + firstName + ", lastName= " + lastName + ", gender= " + gender + "]";
	}

}
