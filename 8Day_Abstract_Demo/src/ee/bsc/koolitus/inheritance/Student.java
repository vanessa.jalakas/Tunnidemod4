package ee.bsc.koolitus.inheritance;

public class Student extends Human {

	private String grade;

	public Student() {
		super("Mari", "");
	}

	public Student(String firstName, String lastName, String grade) {
		super(firstName, lastName);
		this.grade = grade;

	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	@Override
	public String toString() {
		return "Human[firstname=" + firstName + ", lastName= " + lastName + ", gender= " + getGender() + ", grade= "
				+ grade + "]";
	}

}
