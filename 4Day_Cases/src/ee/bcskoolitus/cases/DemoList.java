package ee.bcskoolitus.cases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DemoList {

	public static void main(String[] args) {
		List<String> fruits = new ArrayList<>();
		fruits.add("apple");
		fruits.add("orane");
		fruits.add("banana");
		fruits.add((fruits.indexOf("apple")+1), "kiwi");
		
		fruits.set(2, "orange");
		fruits.remove(1);
		
		for (String fruit : fruits) {
			System.out.println(fruit);
		}

//		for (int i = 0; i < fruits.size(); i++) {
//			System.out.println(fruits.get(i));
//		}
		
		System.out.println("is there banana? " + fruits.contains("banana"));
		System.out.println("is there strawberry? " + fruits.contains("strawberry"));
		
		fruits.addAll(Arrays.asList("coconut", "pineapple", "cherry"));
		
		System.out.println(fruits);
	}

}
