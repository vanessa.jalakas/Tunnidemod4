package ee.bcskoolitus.cases;

public class ArraysDemo {
	public static void main(String[] args) {
		int[] numberArray1 = new int[5];
		 System.out.println("Massiivi pikkus = " + numberArray1.length);
		 System.out.println("Kolmas element ennem väärtustamist = " +
		 numberArray1[2]);
		 numberArray1[1] = 4; numberArray1[2] = 56; numberArray1[4] = 10;
		 System.out.println("Kolmas element pärast väärtustamist = " +
		 numberArray1[2]);
		 System.out.println("---------FOR1------------");
		
		 for(int i = 0; i<numberArray1.length; i++) {
		 System.out.println((i+1) + ". element on " + numberArray1[i]);
		 }
		 System.out.println("---------------------");
		
		 int i = 0;
		 for(; i<numberArray1.length;) {
		 System.out.println((i+1) + ". element on " + numberArray1[i]);
		 i++;
		 }
		 System.out.println("-------WHILE1--------------");
		 int index = 0;
		 while (index<numberArray1.length) {
			 System.out.println((index+1) + ". element on " + numberArray1[index]);
			 index++;
			
		}
		 System.out.println("------DO-WHILE--------------");
		 int index2 = 0;
		 do {
			 System.out.println((index2+1) + ". element on " + numberArray1[index2]);
			 index2++;
			
		} while (index2<numberArray1.length);
		 
		System.out.println("---------FOREACH------------");
		int counter = 1;
		int summa = 0;
		for (int arv : numberArray1) {
			System.out.println(counter + ". element on " + arv);
			summa += arv;
			counter++;
		}System.out.println("numberArray1 summa on " + summa);

		 int[] numbersArray2 = {2,8,42};
		 System.out.println("Massiivi pikkus = " + numbersArray2.length);
		 System.out.println("Kolmas element = " + numbersArray2[2]);
		
		 boolean[] booleansArray = new boolean[8];
		 System.out.println("Massiivi pikkus = " + booleansArray.length);
		 System.out.println("Kolmas element = " + booleansArray[2]);
		
		 char[] charArray = new char[3];
		 System.out.println("Massiivi pikkus = " + charArray.length);
		 System.out.println("Kolmas element = " + charArray[2]);
		
		String[][] stringsArray1 = new String[3][5]; // 3 lines and 5 columns
		System.out.println("Massiivi pikkus on võrdne ridade arvuga = " + stringsArray1.length);
		System.out.println("Massiivi esimese rea pikkus = " + stringsArray1[0].length);
		stringsArray1[0][0] = "abc";
		stringsArray1[0][1] = "ABC";
		stringsArray1[0][4] = "efg";
		String[] secondRow = { "hobune", "koer", "kass", "", null };
		stringsArray1[1] = secondRow;
		
		for (String[] row : stringsArray1) {
			int counter1 = 0;
			for (String column : row) {
				if (counter1 != row.length - 1) {
					System.out.print(column + ", ");

				} else {
					System.out.println(column);

				}
				counter1++;
			}
		}
		System.out.println(
				"esimesest reast: " + stringsArray1[0][2] + ", " + stringsArray1[0][3] + ", " + stringsArray1[0][4]);
		System.out.println(
				"teisest reast: " + stringsArray1[1][2] + ", " + stringsArray1[1][3] + ", " + stringsArray1[1][4]);
		System.out.println("-----------FOR2----------");
		for (int rowIndex = 0; rowIndex < stringsArray1.length; rowIndex++) {
			for (int columnIndex = 0; columnIndex < stringsArray1[rowIndex].length; columnIndex++) {
				if (columnIndex != (stringsArray1[rowIndex].length - 1)) {
					System.out.print(stringsArray1[rowIndex][columnIndex] + ", ");
				} else {
					System.out.println(stringsArray1[rowIndex][columnIndex]);
				}
			}
		
		}
		System.out.println("-----------WHILE2----------");
		int rowIndex = 0;
		while (rowIndex < stringsArray1.length) {
			int columnIndex = 0;
			while (columnIndex < stringsArray1[rowIndex].length) {
				if (columnIndex == (stringsArray1[rowIndex].length - 1)) {
					System.out.println(stringsArray1[rowIndex][columnIndex]);
				} else {
					System.out.print(stringsArray1[rowIndex][columnIndex] + ", ");

				}
				columnIndex++;
			}
			rowIndex++;
		}
		System.out.println("----------DO-WHILE2----------");
		int rowIndex12 = 0;
		do {
			int columnIndex = 0;
			do {
				if (columnIndex == (stringsArray1[rowIndex12].length - 1)) {
					System.out.println(stringsArray1[rowIndex12][columnIndex]);
				} else {
					System.out.print(stringsArray1[rowIndex12][columnIndex] + ", ");
				}
				columnIndex++;

			} while (columnIndex < stringsArray1[rowIndex12].length);
			rowIndex12++;
		} while (rowIndex12 < stringsArray1.length);

	}

}