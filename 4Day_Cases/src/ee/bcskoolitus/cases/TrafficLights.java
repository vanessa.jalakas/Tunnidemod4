package ee.bcskoolitus.cases;

public class TrafficLights {
	public static void main(String[] args) {
		final String LIGHT_GO = "green";
		final String LIGHT_STEDY = "yellow";
		final String LIGHT_STOP = "red";
		
		String color = args[3];
		
		if (color.equalsIgnoreCase(LIGHT_GO)) {
			System.out.println("Driver can drive a car.");
			
		}else if (color.equalsIgnoreCase(LIGHT_STEDY)) {
			System.out.println("Driver has to be ready to stop the car or to start driving");
		}else if (color.equalsIgnoreCase(LIGHT_STOP)) {
			System.out.println("Driver has to stop car and wait for green light");
		} else {
			System.out.println("Mistake made");
		}
		
		
		switch (color.toLowerCase()) {
		case LIGHT_GO:
			System.out.println("Driver can drive a car.");
			break;
			
		case LIGHT_STEDY:
			System.out.println("Driver has to be ready to stop the car or to start driving");
			break;
			
		case LIGHT_STOP:
			System.out.println("Driver has to stop car and wait for green light");
			break;

		default:
			System.out.println("Mistake made");
			break;
		}
		
		
	}

}
