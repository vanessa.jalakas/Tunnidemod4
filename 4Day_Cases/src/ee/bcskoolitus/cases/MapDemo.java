package ee.bcskoolitus.cases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapDemo {

	public static void main(String[] args) {
		Map<Integer, List<String>> studentsInClasses = new HashMap<>();
		studentsInClasses.put(1, Arrays.asList("Mati", "Kati", "Mari"));

		List<String> secondClassStudents = new ArrayList<>();
		secondClassStudents.add("Joosep");
		secondClassStudents.add("Juhan");
		secondClassStudents.add("Elisabet");
		studentsInClasses.put(2, secondClassStudents);

		System.out.println(studentsInClasses);
		System.out.println("---Keys-----");
		for (Integer key : studentsInClasses.keySet()) {
			System.out.println(key);
		}
		System.out.println("---Values-----");
		for (List<String> value : studentsInClasses.values()) {
			System.out.println(value);
		}
		System.out.println("\n---Only second class students-----");
		System.out.println(studentsInClasses.get(2));
		
		secondClassStudents.add("Vanessa");
		studentsInClasses.put(2,secondClassStudents);
		
		studentsInClasses.get(2).add("Eda");
		
		System.out.println(studentsInClasses.get(2));
	}

}
