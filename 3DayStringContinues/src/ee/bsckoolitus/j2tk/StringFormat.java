package ee.bsckoolitus.j2tk;

public class StringFormat {
	public static void main(String[] args) {
		
		String textToFormat = "%s friends play with %s and %s. %<s loves playing with %2$s.";
		System.out.println(String.format(textToFormat, 2, "toys", "Muki"));
		
		//string.format()
		String stringToFormat = "%s. %s is %s years old. %<s years old is %2$s.";
		System.out.println(String.format(stringToFormat, 2, "Vanessa", 22));
		
		String estoniaNamesOfCitys = "This is a list of city names: Tallinn, Tartu, Pärnu, Kuressaare";
		String estonianCitys = estoniaNamesOfCitys.split(":")[1];
		System.out.println(estonianCitys.split(":")[0]);
		String [] arrayOfCityNames = estonianCitys.split(",");
		
		for (String city : arrayOfCityNames) {
			
			System.out.println(city.trim()); //trim on tühikute eemaldamise funkt. Ehk algusest ja lõpust eemaldatakse
			System.out.println(city.replaceAll("[ai]", "iiiiiiiiiii"));
			
		}
		
		
	}

}
