package ee.bsckoolitus.j2tk;

public class ConditionalInt {
	public static void main(String[] arv) {
		final int SISEND_ARV = 18;
		
		if (SISEND_ARV%2 == 0) {
			System.out.println("Sisestatud arv on paaris");
		} else {
			System.out.println("Arv on paaritu");
		}
		
		int sisendArv = Integer.parseInt(arv[1]);
		String arvuOlek = (sisendArv%2 == 0) ? "Sisestatud arv on paaris" : "Arv on paaritu";
		System.out.println("Tingimuslausega: " + arvuOlek);


		
	}

}
