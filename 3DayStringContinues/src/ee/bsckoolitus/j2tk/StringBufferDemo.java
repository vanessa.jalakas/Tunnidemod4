package ee.bsckoolitus.j2tk;

public class StringBufferDemo {
	public static void main(String[] args) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("First line\n");
		stringBuffer.append("Second line\n");
		
		int thirdBeginning = stringBuffer.length();
		
		stringBuffer.insert(thirdBeginning, "Third line\n");
		
		String fourthSentence = "This is the fourth line\n";
		stringBuffer.insert(thirdBeginning, fourthSentence);
		
		System.out.println(stringBuffer);
		
		stringBuffer.delete(thirdBeginning, thirdBeginning + fourthSentence.length());
		
		System.out.println("----------------");
		System.out.println("\tAfter delete:\n" + stringBuffer);
	}

}
