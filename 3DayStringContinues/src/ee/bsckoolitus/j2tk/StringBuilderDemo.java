package ee.bsckoolitus.j2tk;

public class StringBuilderDemo {
	public static void main(String[] args) {
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("This is a first sentence.\n");
		stringBuilder.append("This is second sentence added with append.\n");
		
		int thirdBeginning = stringBuilder.length();
		stringBuilder.insert(thirdBeginning, "This is third sentence added by using insert, is added to the end.\n");
		
		String fourthSentence = "This is fourth sentence that goes before third sentence as using insert.\n";
		stringBuilder.insert(thirdBeginning, fourthSentence);
		
		System.out.println(stringBuilder);
		
		//lets remove fourth sentence, that was added to third start position
		stringBuilder.delete(thirdBeginning, thirdBeginning + fourthSentence.length());
		
		System.out.println(stringBuilder);
		
		}

}
