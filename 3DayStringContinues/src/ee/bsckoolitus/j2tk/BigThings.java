package ee.bsckoolitus.j2tk;

public class BigThings {
	public static void main(String[] args) {
		

		int i = 200; 
		System.out.println(i);
		byte b = ((Integer)i).byteValue();
		
		System.out.println("littleByte = " + b);
		
		long longNumber = 2564897L;
		short shortNumber = ((Long)longNumber).shortValue();
		
		System.out.println("shortNumber = " + shortNumber);
		
		String neli = "44";
		int i1 = Integer.parseInt(neli);
		
		int numberWithUnderscores = 1_202_678;
		System.out.println("numberWithUnderscores = " + numberWithUnderscores );
		
		String oneRandomTextWithNumber12 = "286";
		int numberFromText = Integer.parseInt(oneRandomTextWithNumber12);
		System.out.println("numberFromText = " + numberFromText);
		
//		byte byteFromRandomText = Byte.parseByte(oneRandomTextWithNumber12);
//		System.out.println("numberFromText = " + byteFromRandomText);
		
		short simpleShortNumber = 23;
		String textFromShortNumber = ((Short)simpleShortNumber).toString();
		System.out.println("textFromShortNumber = " + textFromShortNumber);
		
		Long simpleLongNumber = 78_965_430_765L;
		String textFromLongNumber = simpleLongNumber.toString();
		System.out.println("textFromLongNumber = " + textFromLongNumber);
		

		
		
		
	}
	

}
