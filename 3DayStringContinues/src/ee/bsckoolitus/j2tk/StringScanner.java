package ee.bsckoolitus.j2tk;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StringScanner {
	public static void main(String[] args) {
		String scannerText = "Name:Heleen Maibak Name:Henn Sarv Name:Mari Maasikas";

		// separates by spaces
		Scanner scanner = new Scanner(scannerText);
		while (scanner.hasNext()) { // hasNext vaatab, et kas on midagi veel võtta
			System.out.println(scanner.next());

		}
		System.out.println("---------------");
		scanner.close();

		// separates by given word
		Scanner scannerByWord = new Scanner(scannerText);
		scannerByWord.useDelimiter("Name:");
		while (scannerByWord.hasNext()) {
			System.out.println(scannerByWord.next());
		}
		System.out.println("---------------");
		scannerByWord.close();

		// Reads text from file and separates by word "Name:"
		//try with resource
		try (Scanner scannerFile = new Scanner(new File("testFileForScanner.txt"))
				.useDelimiter("Name:")) {
			while (scannerFile.hasNext()) {
				System.out.println(scannerFile.next());
			}
			Logger.getLogger(StringScanner.class.getName()).log(Level.INFO, "Reading file was successful", new Exception());
		} catch (FileNotFoundException ex) {
			//Teeb logi. SEVERE e. ohtlikuse tase. Antud juhul on tegemist eluohtliku veaga
			Logger.getLogger(StringScanner.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

}
