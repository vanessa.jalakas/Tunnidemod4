package ee.bsckoolitus.j2tk;

public class ConditionalStatements {

	public static void main(String[] args) {
		final int ADULT_AGE = 18;

		int katisAge = 15;

		if (katisAge >= ADULT_AGE) {
			System.out.println("Kati is adult");

		} else {
			System.out.println("Kati is not grown up yet");
		}
		
		String isAdultText = (katisAge>=ADULT_AGE) ? "Kati is adult" : "Kati is not grown up yet";
		System.out.println("Tingimuslausega: " + isAdultText);

		
		String[] weather = { "Päike paistab", "Vihma sajab", "Lund sajab" };

		String weatherToday = weather[1];

		if (weatherToday.equals("Päike paistab")) {
			System.out.println("Hea tuju!");

		} else if (weatherToday.equals("Vihma sajab")) {
			System.out.println("Vihmavari!");

		} else if (weatherToday.equals("Lund sajab")) {
			System.out.println("Soojad saapad....");
		}

		else {
			System.out.println("Homme on ka päev");

		}
		
		
	}

}
