package ee.bsc.koolitus.animal;

public class Dog extends Animal {

	private String sleepingPlace;

	public String getSleepingPlace() {
		return sleepingPlace;
	}

	public void setSleepingPlace(String sleepingPlace) {
		this.sleepingPlace = sleepingPlace;
	}

	@Override
	public void moveAhead() {
		System.out.println("Running lazily");
	}

	@Override
	public String toString() {
		return "Dog[" + getName() + " with gender " + getGender() + " sleeps in " + sleepingPlace + "]";
	}

}
