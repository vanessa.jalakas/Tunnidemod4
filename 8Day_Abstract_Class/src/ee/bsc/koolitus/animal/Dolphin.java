package ee.bsc.koolitus.animal;

public class Dolphin extends Animal {
	private ActiveBrainSide activeSide;

	public ActiveBrainSide getActiveSide() {
		return activeSide;
	}

	public void setActiveSide(ActiveBrainSide activeSide) {
		this.activeSide = activeSide;
	}

	@Override
	public void moveAhead() {
		System.out.println("Helping lost swimmers");

	}

	@Override
	public String toString() {
		return "Dolphin[" + getName() + " with gender " + getGender() + " with active " + activeSide
				+ " side of brain]";
	}

}
