package ee.bsc.koolitus.animal;

public class Cat extends Animal {
	private Integer sleepingHours;

	public Integer getSleepingHours() {
		return sleepingHours;
	}

	public void setSleepingHours(Integer sleepingHours) {
		this.sleepingHours = sleepingHours;
	}

	@Override
	public void moveAhead() {
		System.out.println("Cat runs like a cheetah");

	}

	@Override
	public String toString() {
		return "Cat[" + getName() + " with gender " + getGender() + " sleeps " + sleepingHours + "hours]";
	}

}
