package ee.bsc.koolitus.main;

import ee.bsc.koolitus.animal.ActiveBrainSide;
import ee.bsc.koolitus.animal.Cat;
import ee.bsc.koolitus.animal.Dog;
import ee.bsc.koolitus.animal.Dolphin;
import ee.bsc.koolitus.animal.Gender;

public class Main {

	public static void main(String[] args) {

		Dog puppy = new Dog();
		puppy.setName("Betty");
		puppy.setGender(Gender.FEMALE);
		puppy.setSleepingPlace("Bed with owner");
		puppy.moveAhead();

		System.out.println(puppy);

		Cat toygle = new Cat();
		toygle.setName("Suuzy");
		toygle.setGender(Gender.FEMALE);
		toygle.setSleepingHours(25);
		toygle.moveAhead();

		System.out.println(toygle);

		Dolphin happy = new Dolphin();
		happy.setName("Happy");
		happy.setGender(Gender.MALE);
		happy.setActiveSide(ActiveBrainSide.RIGHT);
		happy.moveAhead();

		System.out.println(happy);

	}

}
