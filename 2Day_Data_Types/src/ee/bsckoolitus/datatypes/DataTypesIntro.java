package ee.bsckoolitus.datatypes;
/**
 * 
 * @author opilane
 *
 */
public class DataTypesIntro {
	public static void main(String[] args) {
		//2 byte tüüpi muutujad
		byte bit1 = 127;
		byte bit2 = 8;
		
		
		//2 int tüüpi muutujad
		
		
		byte arvutus = (byte)(bit1+bit2);
		
		System.out.println("Esimene byte: " + bit1 + " ja teine: "+bit2);
		System.out.println("Arvutus: "+ (bit1+bit2));
		System.out.println("Byte arvutus: "+(arvutus));
		
		Byte suurArv =127;
		Byte suurArv1 = -18;
		
		System.out.println("Esimene byte: " + suurArv + " ja teine:" + suurArv1);
		
		char t2ht = 'a';
		char t2ht1 = 'B';
		
		System.out.println("Esimene tähtede trükk = " +t2ht +t2ht1);
		System.out.println("Teine tähtede trükk = " +(t2ht +t2ht1));
		char kaheCharSumma = (char) (t2ht+t2ht1);
		System.out.println("Arvutus charidega "+kaheCharSumma);
		
		int arv1 = 5;
		System.out.println("arv1=  "+arv1);
		arv1 +=2;
		System.out.println("peale esimest tehet arv1=  "+arv1);
		
		arv1++;
		System.out.println("Peale teist tehet on arv1= " +arv1);
		
		int a1 = 12;
		int b1 = ++a1;
		System.out.println("a1= " + a1 + "; b1= " + b1);
		
		int a2 = 12;
		int b2 = a1++;
		System.out.println("a2= " + a2 + "; b2= " + b2);
		
		
		int arv11 = 11;
		arv11 +=5; //samaväärne tehtega arv11 = arv11 +5;
		System.out.println("arv11 = "+ arv11);
		
		int a=1; int b=1; int c=3;
		System.out.println("Kui a=b "+ (a==b) + " ja a=c " + (a==c));
		
		a=c;
		System.out.println("Kui a=b "+ (a==b) + " ja uus a=c " + (a==c));
		
		int v = 18 % 3; int f = 19 % 3; int n = 20 % 3; int m = 21 % 3;
		System.out.println(String.format("Jääkidega väärtused: \n%s,\n%s,\n%s,\n%s", v, f, n, m));
		
		System.out.println("karu" == "ka".concat("ru"));
		System.out.println("karu".equals("ka".concat("ru")));
		
	
		
	}
}
