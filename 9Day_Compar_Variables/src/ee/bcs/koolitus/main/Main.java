package ee.bcs.koolitus.main;

import ee.bcs.koolitus.compare.Auto;
import ee.bcs.koolitus.compare.InCorrectNumberOfDoorException;
import ee.bcs.koolitus.compare.KytuseTyyp;
import ee.bcs.koolitus.compare.Mootor;

public class Main {

	public static void main(String[] args) {
		Auto skoda = new Auto();
		skoda.setKohtadeArv(5);
		try {
			skoda.setUsteArv(8);
		} catch (InCorrectNumberOfDoorException e) {
			System.out.println(e.getMessage());
		}
		skoda.setMootor(new Mootor().setKytuseTyyp(KytuseTyyp.DIISEL).setVoimsus(125));

		Auto bmw = new Auto();
		bmw.setKohtadeArv(5);
		try {
			bmw.setUsteArv(5);
		} catch (InCorrectNumberOfDoorException e) {
			System.out.println(e.getMessage());
		}
		bmw.setMootor(new Mootor().setKytuseTyyp(KytuseTyyp.DIISEL).setVoimsus(125));

		System.out.println("skoda equals to bmw ? " + skoda.equals(bmw));
		System.out.println(skoda.hashCode());
		System.out.println(bmw.hashCode());
		System.out.println("skoda hashcode = bmw hashcode ?" + (skoda.hashCode() == bmw.hashCode()));

	}

}
