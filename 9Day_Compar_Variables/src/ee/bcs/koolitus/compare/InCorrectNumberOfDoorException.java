package ee.bcs.koolitus.compare;

public class InCorrectNumberOfDoorException extends Exception {
	
	public InCorrectNumberOfDoorException() {
		super();
	}
	
	public InCorrectNumberOfDoorException(String message) {
		super(message);
	}
	

}
