package ee.bcs.koolitus.compare;

import java.util.Objects;

public class Mootor implements Comparable<Mootor> {
	private int voimsus;
	private KytuseTyyp kytuseTyyp;

	public int getVoimsus() {
		return voimsus;
	}

	public Mootor setVoimsus(int voimsus) {
		this.voimsus = voimsus;
		return this;
	}

	public KytuseTyyp getKytuseTyyp() {
		return kytuseTyyp;
	}

	public Mootor setKytuseTyyp(KytuseTyyp kytuseTyyp) {
		this.kytuseTyyp = kytuseTyyp;
		return this;
	}

	@Override
	public String toString() {
		return "Mootor[voimsus=" + voimsus + ", kytusetyyp=" + kytuseTyyp + "]";
	}

	@Override
	public boolean equals(Object otherObject) {
		Mootor motorType = (Mootor) otherObject;
		return this.getKytuseTyyp() == motorType.getKytuseTyyp() && this.getVoimsus() == motorType.getVoimsus();
	}

	@Override
	public int hashCode() {
		int hashResult = 231;
		hashResult = 31 * hashResult + Objects.hashCode(voimsus);
		hashResult = 31 * hashResult + Objects.hashCode(kytuseTyyp);

		return hashResult;
	}

	@Override
	public int compareTo(Mootor otherEngine) {
		if (this.equals(otherEngine)) {
			return 0;
		} else if (this.kytuseTyyp.compareTo(otherEngine.getKytuseTyyp()) != 0) {
			return this.kytuseTyyp.compareTo(otherEngine.getKytuseTyyp());

		} else if (this.voimsus < otherEngine.getVoimsus()) {
			return -1;

		}
		return 1;
	}

}
