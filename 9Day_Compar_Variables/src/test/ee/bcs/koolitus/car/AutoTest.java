package test.ee.bcs.koolitus.car;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ee.bcs.koolitus.compare.Auto;
import ee.bcs.koolitus.compare.InCorrectNumberOfDoorException;

public class AutoTest {

	private final int OrderSame = -1;
	private final int OrderChange = 1;

	Auto doorBigSeatBig;
	Auto doorSmallSeatSmall;
	Auto doorBigSeatSmall;
	Auto doorSmallSeatBig;

	@Before

	public void SetUp() {

		doorBigSeatBig = new Auto();
		try {
			doorBigSeatBig.setUsteArv(5);
		} catch (InCorrectNumberOfDoorException e) {
			e.printStackTrace();
		}
		doorBigSeatBig.setKohtadeArv(5);
		
		doorSmallSeatSmall = new Auto();
		try {
			doorSmallSeatSmall.setUsteArv(4);
		} catch (InCorrectNumberOfDoorException e) {
			e.printStackTrace();
		}
		doorSmallSeatSmall.setKohtadeArv(4);
		
		doorBigSeatSmall = new Auto();
		try {
			doorBigSeatSmall.setUsteArv(5);
		} catch (InCorrectNumberOfDoorException e) {
			e.printStackTrace();
		}
		doorBigSeatSmall.setKohtadeArv(4);
		
		doorSmallSeatBig = new Auto();
		try {
			doorSmallSeatBig.setUsteArv(4);
		} catch (InCorrectNumberOfDoorException e) {
			e.printStackTrace();
		}
		doorSmallSeatBig.setKohtadeArv(5);
	}

	// Diesel Engine tests
	@Test
	public void testDoorSameSeatSame_OrderSame() {
		Assert.assertEquals(OrderSame, doorBigSeatBig.compareTo(doorBigSeatBig));
	}
	
	@Test
	public void testDoorBiggerSeatBigger_OrderChange() {
		Assert.assertEquals(OrderChange, doorBigSeatBig.compareTo(doorSmallSeatSmall));
	}
	
	@Test
	public void testDoorSmallSeatSmall_OrderSame() {
		Assert.assertEquals(OrderSame, doorSmallSeatBig.compareTo(doorSmallSeatBig));
	}
	
	@Test
	public void testDoorBiggerSeatSame_OrderChange() {
		Assert.assertEquals(OrderChange, doorBigSeatSmall.compareTo(doorSmallSeatSmall));
	}
	
	@Test
	public void testDoorSmallerSeatSame_OrderSame() {
		Assert.assertEquals(OrderSame, doorSmallSeatSmall.compareTo(doorBigSeatSmall));
	}
	
	@Test
	public void testDoorSameSeatBigger_OrderSame() {
		Assert.assertEquals(OrderSame, doorSmallSeatBig.compareTo(doorSmallSeatSmall));
	}
	

}
