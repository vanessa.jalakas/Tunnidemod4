package test.ee.bcs.koolitus.car;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({MootorTest.class, AutoTest.class})
public class AllTests {

}
