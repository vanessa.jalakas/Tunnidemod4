package test.ee.bcs.koolitus.car;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ee.bcs.koolitus.compare.KytuseTyyp;
import ee.bcs.koolitus.compare.Mootor;

public class MootorTest {

	private final int OrderSame = -1;
	private final int OrderChange = 1;

	Mootor dieselEngineSmallPower;
	Mootor dieselEngineBigPower;
	Mootor petrolEngineSmallPower;
	Mootor petrolEngineBigPower;

	@Before

	public void SetUp() {

		dieselEngineSmallPower = new Mootor().setKytuseTyyp(KytuseTyyp.DIISEL).setVoimsus(50);
		dieselEngineBigPower = new Mootor().setKytuseTyyp(KytuseTyyp.DIISEL).setVoimsus(100);

		petrolEngineSmallPower = new Mootor().setKytuseTyyp(KytuseTyyp.BENSIIN).setVoimsus(50);
		petrolEngineBigPower = new Mootor().setKytuseTyyp(KytuseTyyp.BENSIIN).setVoimsus(100);
	}

	// Diesel Engine tests
	@Test

	public void testDieselEnginePowerSmallerOrderSame() {
		Assert.assertEquals(OrderSame, dieselEngineSmallPower.compareTo(dieselEngineBigPower));

	}

	@Test

	public void testDieselEnginePowerBiggerOrderChange() {
		Assert.assertEquals(OrderChange, dieselEngineBigPower.compareTo(dieselEngineSmallPower));

	}

	// Petrol Engine tests
	@Test

	public void testPetrolEnginePowerSmallerOrderSame() {
		Assert.assertEquals(OrderSame, petrolEngineSmallPower.compareTo(petrolEngineBigPower));

	}

	@Test

	public void testPetrolEnginePowerBiggerOrderChange() {
		Assert.assertEquals(OrderChange, petrolEngineBigPower.compareTo(petrolEngineSmallPower));

	}

	// Petrol and diesel Engine tests

	@Test

	public void testDieselPetrolEnginePowerOrderChange() {
		Assert.assertEquals(OrderChange, dieselEngineSmallPower.compareTo(petrolEngineSmallPower));

	}

	@Test

	public void testPetrolDieselEnginePowerOrderSame() {
		Assert.assertEquals(OrderSame, petrolEngineSmallPower.compareTo(dieselEngineSmallPower));

	}

	@Test

	public void testPetrolDieselEnginePowerBiggerOrderSame() {
		Assert.assertEquals(OrderSame, petrolEngineBigPower.compareTo(dieselEngineSmallPower));

	}

	@Test

	public void testDieselPetrolEngineePowerBiggerOrderChange() {
		Assert.assertEquals(OrderChange, dieselEngineSmallPower.compareTo(petrolEngineBigPower));

	}

}
