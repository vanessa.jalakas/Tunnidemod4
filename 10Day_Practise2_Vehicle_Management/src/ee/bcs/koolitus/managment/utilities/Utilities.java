package ee.bcs.koolitus.managment.utilities;

import java.util.Random;

public class Utilities {
	private final String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private String alphNr = "";
	private String regNr;

	public Utilities teeAutoRegNr() {

		Random random = new Random();
		int randomTriple = random.nextInt(900) + 100;

		for (int i = 0; i < 3; i++) {
			alphNr += alphabet.charAt(random.nextInt(alphabet.length()));
		}
		regNr = ((Integer) randomTriple).toString() + alphNr;
		return this;

	}

	public Utilities teeMootorratasRegNr() {

		Random random = new Random();
		int randomTriple = random.nextInt(90) + 10;

		for (int i = 0; i < 2; i++) {
			alphNr += alphabet.charAt(random.nextInt(alphabet.length()));
		}
		regNr = ((Integer) randomTriple).toString() + alphNr;
		return this;

	}

	public Utilities teeVeesoidukRegNr() {

		Random random = new Random();
		int randomTriple = random.nextInt(900) + 100;

		for (int i = 0; i < 3; i++) {
			alphNr += alphabet.charAt(random.nextInt(alphabet.length()));
		}
		regNr = ((Integer) randomTriple).toString() + "-" + alphNr;
		return this;

	}
	
	@Override
	public String toString() {
		return regNr;
	}

}
