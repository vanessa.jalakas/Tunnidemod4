package ee.bcs.koolitus.managment.main;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ee.bcs.koolitus.managment.utilities.Utilities;
import ee.bcs.koolitus.managment.vehicle.Auto;
import ee.bcs.koolitus.managment.vehicle.AutoTyyp;
import ee.bcs.koolitus.managment.vehicle.Inimene;
import ee.bcs.koolitus.managment.vehicle.JuhiloaKategooria;
import ee.bcs.koolitus.managment.vehicle.LiikumisVahendimp;
import ee.bcs.koolitus.managment.vehicle.Veesoiduk;

public class Main {
	
	public static List<LiikumisVahendimp> lisaUusAutoTyyp(LiikumisVahendimp uusSoiduvahend, List<LiikumisVahendimp> listSoiduvahendid) {
		if (listSoiduvahendid.isEmpty()) {
			listSoiduvahendid.add(uusSoiduvahend);
		}
		else {
			for (LiikumisVahendimp vanaSoiduvahend : listSoiduvahendid) {
				if(vanaSoiduvahend.equals(uusSoiduvahend)){
					System.out.println("Sellise tyybiga auto on listis");
				}else {
					listSoiduvahendid.add(uusSoiduvahend);
				}
			}
		}

		return listSoiduvahendid;
	}
	
		
	public static void main(String[] args) {
		final List<LiikumisVahendimp> listUnikaalsedAutod = new ArrayList<LiikumisVahendimp>();
		
		Inimene inimene = new Inimene();
		Date date = new Date();
		inimene.setSynnikuupaev(date);
		
		
		Auto bmw = new Auto();
		bmw.setRegNumber(new Utilities().teeAutoRegNr());
		bmw.setNoutudJuhiloaKategooria(JuhiloaKategooria.B);
		bmw.looUnikaalneID();
		bmw.setAutoTyyp(AutoTyyp.SÕIDUAUTO);
		LiikumisVahendimp bmw1 = (LiikumisVahendimp)bmw;
		
		Auto skoda = new Auto();
		skoda.setRegNumber(new Utilities().teeAutoRegNr());
		skoda.setNoutudJuhiloaKategooria(JuhiloaKategooria.B);
		skoda.looUnikaalneID();
		skoda.setAutoTyyp(AutoTyyp.SÕIDUAUTO);
		LiikumisVahendimp skoda1 = (LiikumisVahendimp)skoda;
		
		System.out.println(bmw);
		
		//Need some casting
		Veesoiduk linda = new Veesoiduk();
		linda.setRegNumber(new Utilities().teeVeesoidukRegNr());
		LiikumisVahendimp linda1 = (LiikumisVahendimp)linda;
		
		System.out.println(linda);
		

		
		lisaUusAutoTyyp(bmw1,listUnikaalsedAutod);
		lisaUusAutoTyyp(skoda1,listUnikaalsedAutod);
		
		
		System.out.println("List: " + listUnikaalsedAutod);
	}

}
