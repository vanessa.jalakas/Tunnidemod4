package ee.bcs.koolitus.managment.vehicle;

import java.util.HashMap;
import java.util.Objects;

public class LiikumisVahendimp implements LiikumisVahendInt {

	
	public LiikumisVahendimp() {

	}
	
//	public void omanikud(HashMap<Inimene> omanik) {
//		
//	}


	@Override
	public int looUnikaalneID() {
		return hashCode();
		
	}
	
	@Override
	public void soidab(String algus, String lopp) {
		System.out.println(String.format("Liikumisvahend soidab %s'st %s'sse.", algus, lopp));
	}
	
	@Override
	public int hashCode(){
		Inimene inimene = new Inimene();
		int hash = 121;
		hash = 31 * hash + Objects.hash(inimene.getEesnimi());
	    return hash;
	}
	

}
