package ee.bcs.koolitus.managment.vehicle;

import ee.bcs.koolitus.managment.utilities.Utilities;

public class Mootorratas extends Mootorsoiduk{
	
	private Utilities regNumber;
	
	public Mootorratas() {
		
	}
	
	public Utilities getRegNumber() {
		return regNumber;
	}
	public void setRegNumber(Utilities regNumber) {
		this.regNumber = regNumber;
	}
	
	
}
