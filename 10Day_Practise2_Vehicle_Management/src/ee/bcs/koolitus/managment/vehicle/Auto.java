package ee.bcs.koolitus.managment.vehicle;

import ee.bcs.koolitus.managment.utilities.Utilities;

public class Auto extends Mootorsoiduk implements Comparable<Auto>{
	private AutoTyyp autoTyyp;
	private Utilities regNumber;
	

	public Auto() {
		
	}
	
	public Utilities getRegNumber() {
		return regNumber;
	}
	public void setRegNumber(Utilities regNumber) {
		this.regNumber = regNumber;
	}
	
	public AutoTyyp getAutoTyyp() {
		return autoTyyp;
	}
	
	public Auto setAutoTyyp(AutoTyyp autoTyyp) {
		this.autoTyyp = autoTyyp;
		return this;
	}
	


	@Override
	public String toString() {
		return "Auto[Registreerimisnumbriga: " + regNumber + ", Tyybiga: " + autoTyyp + "]";
	}
	
	@Override
	public boolean equals(Object otherObject) {
		if(this == otherObject) {
			return true;
		}
		if (otherObject==null) {
			return false;
		}
		if (getClass() != otherObject.getClass()) {
			return false;
		}
		final Auto otherVehicle = (Auto) otherObject;
		return this.getAutoTyyp() == otherVehicle.getAutoTyyp();
	}
	
	
	@Override
	public int compareTo(Auto auto) {
		if (this.autoTyyp.equals(auto.getAutoTyyp()))
			return -1;

		return 1;
	}

}
