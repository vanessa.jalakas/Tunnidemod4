package ee.bcs.koolitus.managment.vehicle;

public interface LiikumisVahendInt {

	void soidab(String algus, String lopp);
	int looUnikaalneID();

}
