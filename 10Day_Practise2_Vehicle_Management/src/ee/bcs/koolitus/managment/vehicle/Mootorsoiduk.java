package ee.bcs.koolitus.managment.vehicle;



public class Mootorsoiduk extends LiikumisVahendimp {
	
	
	private JuhiloaKategooria noutudJuhiloaKategooria;
	
	public Mootorsoiduk() {
		
	}
	
	public boolean kaivutub() {
		System.out.println("Machine start");
		return true;
	}
	
	public boolean seiskub() {
		System.out.println("Machine turn off");
		return true;
	}
	
	public JuhiloaKategooria getNoutudJuhiloaKategooria() {
		return noutudJuhiloaKategooria;
	}
	public void setNoutudJuhiloaKategooria(JuhiloaKategooria noutudJuhiloaKategooria) {
		this.noutudJuhiloaKategooria = noutudJuhiloaKategooria;
	}
	
	

}
