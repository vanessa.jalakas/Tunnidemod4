package ee.bcs.koolitus.managment.vehicle;

import ee.bcs.koolitus.managment.utilities.Utilities;

public class Veesoiduk extends Mootorsoiduk implements Comparable<Veesoiduk> {
	private VeesoidukiTyyp veesoidukiTyyp;
	private Utilities regNumber;

	public VeesoidukiTyyp getVeesoidukiTyyp() {
		return veesoidukiTyyp;
	}

	public void setVeesoidukiTyyp(VeesoidukiTyyp veesoidukiTyyp) {
		this.veesoidukiTyyp = veesoidukiTyyp;
	}
	
	public Utilities getRegNumber() {
		return regNumber;
	}
	public void setRegNumber(Utilities regNumber) {
		this.regNumber = regNumber;
	}

	
	@Override
	public String toString() {
		return "Auto[Registreerimisnumbriga: " + regNumber + ", Tyybiga: " + veesoidukiTyyp + "]";
	}
	
	@Override
	public boolean equals(Object otherObject) {
		if(this == otherObject) {
			return true;
		}
		if (otherObject==null) {
			return false;
		}
		if (getClass() != otherObject.getClass()) {
			return false;
		}
		final Veesoiduk otherVehicle = (Veesoiduk) otherObject;
		return this.getVeesoidukiTyyp() == otherVehicle.getVeesoidukiTyyp();
	}
	
	
	@Override
	public int compareTo(Veesoiduk veesoiduk) {
		if (this.veesoidukiTyyp.equals(veesoiduk.getVeesoidukiTyyp()))
			return -1;

		return 1;
	}

}
