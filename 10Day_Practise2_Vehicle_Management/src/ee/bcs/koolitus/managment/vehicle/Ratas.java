package ee.bcs.koolitus.managment.vehicle;

public class Ratas {
	private int kaikudeArv;
	private String seeriaNr;
	private IstumisAsend istumisAsend;
	
	public int getKaikudearv() {
		return kaikudeArv;
	}
	
	public Ratas setKaikudeArv() {
		this.kaikudeArv = kaikudeArv;
		return this;
	}

	public String getSeeriaNr() {
		return seeriaNr;
	}

	public void setSeeriaNr(String seeriaNr) {
		this.seeriaNr = seeriaNr;
	}
	
	public IstumisAsend getIstumisasend() {
		return istumisAsend;
	}
	
	public Ratas setIstumisasend(IstumisAsend istumisAsend) {
		this.istumisAsend = istumisAsend;
		return this;
	}
	
}
