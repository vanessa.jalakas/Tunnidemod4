package ee.bcs.koolitus.managment.vehicle;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Inimene {
	private String eesnimi;
	private String perenimi;
	private Date synnikuupaev;
	private final List<JuhiloaKategooria> listJuhiloakategooria = new ArrayList();
	
	public List<JuhiloaKategooria> lisaJuhiloaKategooria(JuhiloaKategooria kategooria){
		listJuhiloakategooria.add(kategooria);
		return listJuhiloakategooria;
	}
	
	public String getEesnimi() {
		return eesnimi;
	}
	public void setEesnimi(String inimene) {
		this.eesnimi = eesnimi;
	}
	public String getPerenimi() {
		return perenimi;
	}
	public void setPerenimi(String perenimi) {
		this.perenimi = perenimi;
	}
	public Date getSynnikuupaev() {
		return synnikuupaev;
	}
	public void setSynnikuupaev(Date synnikuupaev) {
		this.synnikuupaev = synnikuupaev;
	}
	public List<JuhiloaKategooria> getJuhiloakategooria() {
		return listJuhiloakategooria;
	}
	
	

}
