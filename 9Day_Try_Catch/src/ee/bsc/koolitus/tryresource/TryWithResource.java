package ee.bsc.koolitus.tryresource;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TryWithResource {

	public static void main(String[] args) {
		try(FileReader reader = new FileReader(new File("notExistingFile.txt"));
				FileWriter writer = new FileWriter(new File("output.txt"))){
			char[] fileContent = new char[500];
			//reader.read(fileContent);
			
			int counter = 1;
			while(counter<10) {
				writer.append("there were" + counter +"students\n");
				counter++;
			}
			writer.flush();
		} catch (FileNotFoundException e) { //Kõige detailsem on esimesena
			e.printStackTrace();
		} catch (IOException e) { //viimasena kõige üldisem
			e.printStackTrace();
		}

	}

}
