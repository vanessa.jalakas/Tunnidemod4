package ee.bsc.koolitus.auto;

public class Auto {
	private int usteArv;
	private int kohtadeArv;
	private Mootor mootor;
	private VotmeTyyp votiValik;
	private String voti;
	private int id;
	private static int idCounter = 0;

	
	// default constructor
	public Auto() {
		idCounter++;
		id = idCounter;
		System.out.println("Tegin uue auto objekti");
	}

	public Auto(Mootor mootor) {
		this(); // Links to Auto constructor
		this.mootor = mootor;
	}
	public VotmeTyyp getVotiValik() {
		return votiValik;
	}

	public void setVotiValik(VotmeTyyp votiValik) {
		this.votiValik = votiValik;
	}

	public void setVotiValik(VotmeTyyp vanaVotiValik, VotmeTyyp uusVotiValik) {
		if (this.votiValik.equals(uusVotiValik)) {
			this.votiValik = uusVotiValik;
		} else {
			System.out.println("Vale valik");
		}

	}
	
	public String getVoti() {
		return voti;
	}

	public void setVoti(String voti) {
		this.voti = voti;
	}

	public void setVoti(String vanaVoti, String uusVoti) {
		if (vanaVoti.equals(this.voti)) {
			this.voti = uusVoti;
		} else {
			System.out.println("Vana võti ei sobi");
		}

	}

	public void kaivitaMootor() {
		if (mootor.getKytuseTyyp().equals(KytuseTyyp.DIISEL)) {
			System.out.println("Käivita eelsüüde");

		}
		System.out.println("Käivita starter");
	}

	public void kaivitaMootor(String voti) {
		if (voti.equals(this.voti)) {
			System.out.println("Pane võti süütelukku");
			kaivitaMootor();
		}else {
			System.out.println("Vale auto");
		}
	}

	public int getUsteArv() {
		return usteArv;
	}

	public void setUsteArv(int usteArv) {
		this.usteArv = usteArv;
	}

	public int getKohtadeArv() {
		return kohtadeArv;
	}

	public void setKohtadeArv(int kohtadeArv) {
		this.kohtadeArv = kohtadeArv;
	}

	public Mootor getMootor() {
		return mootor;
	}

	public void setMootor(Mootor mootor) {
		this.mootor = mootor;
	}
	
	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Auto[counter=" + id + ", usteArv=" + usteArv + ", kohtadeArv=" + kohtadeArv + ", mootor=" + mootor + ", voti=" + voti + "]";
	}

}
