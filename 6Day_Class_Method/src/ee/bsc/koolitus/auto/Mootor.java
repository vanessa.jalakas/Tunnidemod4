package ee.bsc.koolitus.auto;

public class Mootor {
	private int voimsus;
	private KytuseTyyp kytuseTyyp;

	public int getVoimsus() {
		return voimsus;
	}

	public Mootor setVoimsus(int voimsus) {
		this.voimsus = voimsus;
		return this;
	}

	public KytuseTyyp getKytuseTyyp() {
		return kytuseTyyp;
	}

	public Mootor setKytuseTyyp(KytuseTyyp kytuseTyyp) {
		this.kytuseTyyp = kytuseTyyp;
		return this;
	}

	@Override
	public String toString() {
		return "Mootor[voimsus=" + voimsus + ", kytusetyyp=" + kytuseTyyp + "]";
	}

}
