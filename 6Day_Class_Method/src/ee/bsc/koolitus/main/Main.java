package ee.bsc.koolitus.main;

import ee.bsc.koolitus.auto.Auto;
import ee.bsc.koolitus.auto.KytuseTyyp;
import ee.bsc.koolitus.auto.Mootor;
import ee.bsc.koolitus.auto.VotmeTyyp;

public class Main {

	public static void main(String[] args) {
		Mootor diiselMootor = new Mootor().setKytuseTyyp(KytuseTyyp.DIISEL).setVoimsus(125);
		System.out.println(diiselMootor.getClass().getSimpleName()); // print out Mootor

		// diiselMootor.setKytuseTyyp(KytuseTyyp.DIISEL).setVoimsus(125);

		Auto skoda = new Auto(diiselMootor);
		skoda.setKohtadeArv(5);
		skoda.setUsteArv(5);
		// skoda.setMootor(diiselMootor);

		System.out.println("auto = " + skoda);
		System.out.println("mootor = " + diiselMootor);

		Auto bmw = new Auto();
		bmw.setKohtadeArv(2);
		bmw.setUsteArv(3);
		bmw.setMootor(new Mootor().setKytuseTyyp(KytuseTyyp.BENSIIN).setVoimsus(125));
		skoda.setVoti("bmwKey1");
		System.out.println("----votme komtroll-----");
		bmw.setVoti("bmwKey1VALE", "UUSbmwVoti");

		System.out.println("Auto 2: " + bmw);

		// skoda.kaivitaMootor("bmwKEY1", KytuseTyyp.BENSIIN);
		// bmw.kaivitaMootor(KytuseTyyp.DIISEL);

		System.out.print("skoda: ");
		skoda.kaivitaMootor("bmwKey1");
		System.out.print("bmw: ");
		bmw.kaivitaMootor();

		System.out.println("----vw----");
		Auto vw = new Auto();
		vw.setVotiValik(VotmeTyyp.SIGNAL_KEY);
		vw.setVotiValik(VotmeTyyp.KEY, VotmeTyyp.SIGNAL);

	}

}
