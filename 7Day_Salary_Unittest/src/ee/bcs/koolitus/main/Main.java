package ee.bcs.koolitus.main;

import java.math.BigDecimal;

import ee.bsc.koolitus.salary.unittest.Brutopalk;

public class Main {

	public static void main(String[] args) {
		Brutopalk salary = new Brutopalk();
		salary.setSalary(BigDecimal.valueOf(-1126.67));
		System.out.println("Net salary = " + salary.netSalary());
		System.out.println("Social tax = " + salary.socialTaxCalc());

	}

}
