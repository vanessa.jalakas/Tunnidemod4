package ee.bsc.koolitus.salary.unittest;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Brutopalk {

	private BigDecimal salary = null;

	final BigDecimal TAX_FREE = BigDecimal.valueOf(0.8);
	final BigDecimal TAX_FREE_SUM1000 = new BigDecimal(500);
	final BigDecimal TAX_FREE_SUM_LESS_2000 = new BigDecimal(250);
	final BigDecimal TAX_FREE_SUM_BIGGER_2000 = new BigDecimal(0);

	public BigDecimal netSalaryCalc(BigDecimal taxFreeSum) {
		return ((salary.subtract(taxFreeSum)).multiply(TAX_FREE).add(taxFreeSum)).setScale(2, RoundingMode.HALF_UP);
	}

	public BigDecimal netSalary() {
		BigDecimal netSalary = null;
		if (salary.compareTo(BigDecimal.valueOf(500)) < 0) {
			netSalary = this.salary;
		}
			else if (salary.compareTo(BigDecimal.valueOf(1000)) <= 0) {
			netSalary = netSalaryCalc(TAX_FREE_SUM1000);

		} else if (salary.compareTo(BigDecimal.valueOf(2000)) < 0) {

			netSalary = netSalaryCalc(TAX_FREE_SUM_LESS_2000);
		} else if  (salary.compareTo(BigDecimal.valueOf(2000)) >= 0){
			netSalary = netSalaryCalc(TAX_FREE_SUM_BIGGER_2000);
		}
		return netSalary.setScale(2, RoundingMode.HALF_UP);
	}

	public BigDecimal socialTaxCalc() {
		BigDecimal socialTax = salary.multiply(BigDecimal.valueOf(0.3));
		return socialTax.setScale(2, RoundingMode.HALF_UP);
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public Brutopalk setSalary(BigDecimal salary) throws IllegalArgumentException {
		if (salary.compareTo(BigDecimal.ZERO) > 0) {
			this.salary = salary;
		} else {
			throw new IllegalArgumentException();
		}
		return this;
	}
}
