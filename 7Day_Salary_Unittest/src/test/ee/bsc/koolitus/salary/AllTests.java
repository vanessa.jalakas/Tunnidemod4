package test.ee.bsc.koolitus.salary;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({SalaryTest.class})
public class AllTests {
	

}
