package test.ee.bsc.koolitus.salary;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Assert;
import org.junit.Test;

import ee.bsc.koolitus.salary.unittest.Brutopalk;

public class SalaryTest {

	// ---SocialTax-Test---
	@Test
	public void testSocialTaxWithExactValue() {
		BigDecimal givenGrossSalary = BigDecimal.valueOf(1000);
		BigDecimal expectedSocialTax = BigDecimal.valueOf(300);

		Brutopalk testSalary = new Brutopalk();
		testSalary.setSalary(givenGrossSalary);
		Assert.assertEquals(expectedSocialTax.setScale(2, RoundingMode.HALF_UP), testSalary.socialTaxCalc());
	}

	@Test
	public void testSocialTaxWithCommaValue() {
		BigDecimal givenGrossSalary = BigDecimal.valueOf(777.87);
		BigDecimal expectedSocialTax = BigDecimal.valueOf(233.36);
		Brutopalk testSalary = new Brutopalk();
		testSalary.setSalary(givenGrossSalary);
		Assert.assertEquals(expectedSocialTax, testSalary.socialTaxCalc()); // Assert - kontrolli, et - assertEquals -
																			// oleks võrdne
	}

	// ---SetSalary-Test---
	@Test(expected = IllegalArgumentException.class)
	public void testSetNegativeGrossSalary() {
		BigDecimal negativeGrossSalary = BigDecimal.valueOf(-1000);
		Brutopalk testSalary = new Brutopalk();
		testSalary.setSalary(negativeGrossSalary);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetZeroGrossSalary() {
		BigDecimal zeroGrossSalary = BigDecimal.valueOf(0);
		Brutopalk testSalary = new Brutopalk();
		testSalary.setSalary(zeroGrossSalary);
	}

	@Test
	public void testSetPositiveGrossSalary() {
		BigDecimal positiveGrossSalary = BigDecimal.valueOf(1000);
		Brutopalk testSalary = new Brutopalk();
		testSalary.setSalary(positiveGrossSalary);

		Assert.assertEquals(positiveGrossSalary, testSalary.getSalary());
	}

	// ---netSalaryCalc-Test---

	@Test
	public void testNetSalaryCalc() {
		BigDecimal givenGrossSalary = BigDecimal.valueOf(1000);
		BigDecimal expectedNetSalary = BigDecimal.valueOf(900);
		Brutopalk testNetSalary = new Brutopalk();
		testNetSalary.setSalary(givenGrossSalary);

		Assert.assertEquals(expectedNetSalary.setScale(2, RoundingMode.HALF_UP),
				testNetSalary.netSalaryCalc(BigDecimal.valueOf(500)));
	}
	
	// ---netSalary-Test---
	// --- 0 < Brutopalk < 1000 ---
	// --- Tax = 500 ---
//	@Test(expected = IllegalArgumentException.class)
//	public void testNetSalaryWhenZero() {
//		BigDecimal zeroGrossSalary = BigDecimal.valueOf(0);
//		Brutopalk testSalary = new Brutopalk();
//		testSalary.setSalary(zeroGrossSalary);
//	}
	
	@Test
	public void testNetSalaryWhen_999() {
		BigDecimal givenGrossSalary_999 = BigDecimal.valueOf(999);
		BigDecimal expepectedNetSalary = BigDecimal.valueOf(899.2).setScale(2, RoundingMode.HALF_UP);
		Brutopalk testNetSalary = new Brutopalk();
		testNetSalary.setSalary(givenGrossSalary_999);
		
		Assert.assertEquals(expepectedNetSalary, testNetSalary.netSalary());
		
	}
}
