package ee.bsc.koolitus.inter;

public class DogInt extends Animal {

	private String sleepingPlace;

	public String getSleepingPlace() {
		return sleepingPlace;
	}

	public void setSleepingPlace(String sleepingPlace) {
		this.sleepingPlace = sleepingPlace;
	}

	@Override
	public void moveAhead() {
		System.out.println("Chasing seagulls");
	}

}
