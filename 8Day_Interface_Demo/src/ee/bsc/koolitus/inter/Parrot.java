package ee.bsc.koolitus.inter;

public class Parrot implements AnimalInter, Bird{
	
	@Override
	public void moveAhead() {
		System.out.println("Flying around");
	}
	
	@Override
	public void singing() {
		System.out.println("Shrieks");
	}
	
	@Override
	public void moulting() {
		System.out.println("Looks nice again");
	}

}
