package ee.bsc.koolitus.inter;

public abstract class Animal implements AnimalInter {
	private String name;
	private Gender gender;

	public abstract void moveAhead(); // Tuleb lapsklassides defineerida

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

}
