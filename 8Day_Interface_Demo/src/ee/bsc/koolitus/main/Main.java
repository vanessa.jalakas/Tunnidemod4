package ee.bsc.koolitus.main;

import ee.bsc.koolitus.inter.DogInt;
import ee.bsc.koolitus.inter.Gender;
import ee.bsc.koolitus.inter.Parrot;

public class Main {

	public static void main(String[] args) {
		Parrot jack = new Parrot();
		jack.moveAhead();
		jack.moulting();
		jack.singing();
		
		
		DogInt kiki = new DogInt();
		kiki.setName("Kiki");
		kiki.setGender(Gender.FEMALE);
		kiki.setSleepingPlace("pillow");
		kiki.moveAhead();
		
		
		
	}

}
