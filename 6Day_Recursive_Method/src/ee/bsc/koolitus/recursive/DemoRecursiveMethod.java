package ee.bsc.koolitus.recursive;

public class DemoRecursiveMethod {

	public static void main(String[] args) {
		System.out.println(factorial(3));
		System.out.println("Sum of numbers: " + sumVarargs(1, 23, 45));
		System.out.println(averageOfFamilyAge("Jalakas", 22, 49, 33, 27));

	}

	public static int factorial(int x) {
		System.out.println(x);
		if (x <= 1) {
			return x;
		} else {
			return factorial(x - 1) * x;
		}
	}

	public static double factorial(double y) {
		return (y <= 1) ? y : factorial(y - 1) * y;
	}

	public static int sumVarargs(int... numbers) {
		int sum = 0;
		for (int num : numbers) {
			sum += num;
		}
		return sum;

	}

	public static String averageOfFamilyAge(String family, int... numbers) {

		return String.format("Family of %s average age is %s. Family members amount is %s", family,
				sumVarargs(numbers) / numbers.length, numbers.length);

	}

}
