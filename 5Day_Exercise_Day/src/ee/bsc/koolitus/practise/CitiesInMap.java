package ee.bsc.koolitus.practise;

import java.util.TreeMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CitiesInMap {

	public static void main(String[] args) {

		Map<String, List<String>> citiesNames = new TreeMap<>();

		citiesNames.put("Soome", new ArrayList<>(Arrays.asList("Helsinki", "Helsingi", "Helsinki")));
		citiesNames.put("Läti", new ArrayList<>(Arrays.asList("Riga", "Riia", "Riga")));
		citiesNames.put("Eesti", new ArrayList<>(Arrays.asList("Tallinn", "Tallinn", "Tallinn")));
		citiesNames.put("Hispaania", new ArrayList<>(Arrays.asList("Madrid", "Madrid", "Madrid")));
		citiesNames.put("Norra", new ArrayList<>(Arrays.asList("Oslo", "Oslo", "Oslo")));

		System.out.println("---ul4---");
		for (List<String> cityList : citiesNames.values()) {
			System.out.println(cityList.get(1));

		}
		System.out.println("---ul5---");
		for (String cityKey : citiesNames.keySet()) {
			for (List<String> cityValue : citiesNames.values()) {
				System.out.println(String.format("Riik - %s: pealinn - %s; inglise keeles - %s; kohalikus keeles - %s ",
						cityKey, cityValue.get(1), cityValue.get(0), cityValue.get(2)));
				break;

			}

		}
		System.out.println("---ul5.ver2---");
		for (String cityKey : citiesNames.keySet()) {
			List<String> cityValues = citiesNames.get(cityKey);
			System.out.println(String.format("Riik - %s: pealinn - %s; inglise keeles - %s; kohalikus keeles - %s ",
					cityKey, cityValues.get(1), cityValues.get(0), cityValues.get(2)));
		}
		
		System.out.println("---ul6---");
		
			
		
		


	}

}
