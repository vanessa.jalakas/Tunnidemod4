package ee.bsc.koolitus.practise;

public class CitiesInArray {

	private static void PrintCities(String[][] ArrayName) {
		for (int rowIndex = 0; rowIndex < ArrayName.length; rowIndex++) {
			if (ArrayName[rowIndex][0] != null && ArrayName[rowIndex][0] != "") {
				String country = ArrayName[rowIndex][0];
				String capitalCityEstonain = ArrayName[rowIndex][2];
				String capitalCityEnglish = ArrayName[rowIndex][1];
				String cityLocal = ArrayName[rowIndex][3];
				if (country.contains(":")) {
					System.out.println(
							String.format("Riik - %s: pealinn - %s; inglise keeles - %s; kohalikus keeles - %s ",
									country.split(":")[1], capitalCityEstonain, capitalCityEnglish, cityLocal));
				} else {
					System.out.println(
							String.format("Riik - %s: pealinn - %s; inglise keeles - %s; kohalikus keeles - %s ",
									country, capitalCityEstonain, capitalCityEnglish, cityLocal));

				}
			}
		}
	}

	public static void main(String[] args) {
		String[][] citiesArray = new String[195][4];

		String[] namesFinland = { "Helsinki", "Helsingi", "Helsinki", "Soome" };
		String[] namesOfLatvia = { "Riga", "Riia", "Riga", "Läti" };
		String[] namesOfEstonia = { "Tallinn", "Tallinn", "Tallinn", "Eesti" };
		String[] namesOfSpain = { "Madrid", "Madrid", "Madrid", "Hispaania" };
		String[] namesOfNorway = { "Oslo", "Oslo", "Oslo", "Norra" };

		citiesArray[0] = namesFinland;
		citiesArray[1] = namesOfLatvia;
		citiesArray[2] = namesOfEstonia;
		citiesArray[3] = namesOfSpain;
		citiesArray[4] = namesOfNorway;

		System.out.println("\n-----ul.4-----");

		final int ESTONIAN_CITY_NAMES = 1;

		for (int rowIndex = 0; rowIndex < citiesArray.length; rowIndex++) {
			if (citiesArray[rowIndex][ESTONIAN_CITY_NAMES] != null
					&& citiesArray[rowIndex][ESTONIAN_CITY_NAMES] != "") {
				System.out.println(citiesArray[rowIndex][ESTONIAN_CITY_NAMES]);

			}
		}

		System.out.println("\n-----ul.5-----");

		for (int rowIndex = 0; rowIndex < citiesArray.length; rowIndex++) {
			if (citiesArray[rowIndex][0] != null && citiesArray[rowIndex][0] != "") {
				String country = citiesArray[rowIndex][3];
				String capitalCityEstonain = citiesArray[rowIndex][1];
				String capitalCityEnglish = citiesArray[rowIndex][0];
				String cityLocal = citiesArray[rowIndex][2];

				System.out.println(String.format("Riik - %s: pealinn - %s; inglise keeles - %s; kohalikus keeles - %s ",
						country, capitalCityEstonain, capitalCityEnglish, cityLocal));

			}
		}

		System.out.println("\n-----ul.6-----");
		// Country name into first column
		for (int row = 0; row < citiesArray.length; row++) {
			if (citiesArray[row][ESTONIAN_CITY_NAMES] != null && citiesArray[row][ESTONIAN_CITY_NAMES] != "") {
				String[] newTempRow = { citiesArray[row][3], citiesArray[row][0], citiesArray[row][1],
						citiesArray[row][2] };
				citiesArray[row] = newTempRow;

			}

		}
		// If country have multiple national languages, adding extra city names
		String[] newNamesFinland = { "Soome", "Helsinki", "Helsingi", "Helsinki, Helsingfors" };
		citiesArray[0] = newNamesFinland;

		String[] newNamesOfLatvia = { "Läti", "Riga", "Riia", "Riga, Rīga" };
		citiesArray[1] = newNamesOfLatvia;

		for (int rowIndex = 0; rowIndex < citiesArray.length; rowIndex++) {
			if (citiesArray[rowIndex][0] != null && citiesArray[rowIndex][0] != "") {
				String country = citiesArray[rowIndex][0];
				String capitalCityEstonain = citiesArray[rowIndex][2];
				String capitalCityEnglish = citiesArray[rowIndex][1];
				String cityLocal = citiesArray[rowIndex][3];

				System.out.println(String.format("Riik - %s: pealinn - %s; inglise keeles - %s; kohalikus keeles - %s ",
						country, capitalCityEstonain, capitalCityEnglish, cityLocal));

			}
		}

		System.out.println("\n-----ul.7-----");
		// Country label adding

		for (String[] row : citiesArray) {
			if (row[0] != null && row[0] != "") {
				row[0] = "country:" + row[0];
			}

		}

		System.out.println("\n-----ul.8-----");

		PrintCities(citiesArray);

		System.out.println("\n-----ul.9-----");
		String[] namesKurrunurruvutisaar = { "Kurrunurruvutisaar", "LongStocking City", "Pikksuka linn", "Langstrump" };

		for (int i = citiesArray.length - 1; i >= 2; i--) {

			if (citiesArray[i][0] != null && citiesArray[i][0] != "") {
				citiesArray[i + 1] = citiesArray[i];
			}
			if (i == 2) {
				citiesArray[i] = namesKurrunurruvutisaar;

			}
		}

		PrintCities(citiesArray);
		
		System.out.println("\n-----ul.10-----");
		for (int i = 2; i < citiesArray.length; i++) {
			if (citiesArray[i][0] != null && citiesArray[i][0] != "") {
				citiesArray[i] = citiesArray[i+1];
				
			}
			
		}PrintCities(citiesArray);

	}
	
}
