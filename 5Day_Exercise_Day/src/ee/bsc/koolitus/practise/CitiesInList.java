package ee.bsc.koolitus.practise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CitiesInList {
	private static void PrintCities(List<List<String>> ListName) {
		for (List<String> citiesRow : ListName) {
			if (citiesRow.get(3).contains(":")) {
				System.out.println(String.format("Riik - %s: pealinn - %s; inglise keeles - %s; kohalikus keeles - %s ",
						citiesRow.get(3).split(":")[1], citiesRow.get(1), citiesRow.get(0), citiesRow.get(2)));
			} else {

			
			System.out.println(String.format("Riik - %s: pealinn - %s; inglise keeles - %s; kohalikus keeles - %s ",
					citiesRow.get(3), citiesRow.get(1), citiesRow.get(0), citiesRow.get(2)));
			}
		}

	}

	public static void main(String[] args) {
		List<List<String>> citiesList = new ArrayList<>();

		citiesList.add(new ArrayList<>(Arrays.asList("Helsinki", "Helsingi", "Helsinki", "Soome")));
		citiesList.add(new ArrayList<>(Arrays.asList("Riga", "Riia", "Riga", "Läti")));
		citiesList.add(new ArrayList<>(Arrays.asList("Tallinn", "Tallinn", "Tallinn", "Eesti")));
		citiesList.add(new ArrayList<>(Arrays.asList("Madrid", "Madrid", "Madrid", "Hispaania")));
		citiesList.add(new ArrayList<>(Arrays.asList("Oslo", "Oslo", "Oslo", "Norra")));

		for (List<String> citiesRow : citiesList) {
			System.out.println(citiesRow.get(1));
		}

		for (List<String> citiesRow : citiesList) {
			System.out.println(String.format("Riik - %s: pealinn - %s; inglise keeles - %s; kohalikus keeles - %s ",
					citiesRow.get(3), citiesRow.get(1), citiesRow.get(0), citiesRow.get(2)));

		}
		System.out.println("\t---ex.6---");
		citiesList.get(0).add(2, "Helsingfors");

		citiesList.get(1).add(2, "Rīga");

		for (List<String> citiesRow : citiesList) {
			if (citiesRow.size() > 4) {
				System.out.println(String.format(
						"Riik - %s: pealinn - %s; inglise keeles - %s; kohalikus keeles - %s; %s ", citiesRow.get(4),
						citiesRow.get(1), citiesRow.get(0), citiesRow.get(2), citiesRow.get(3)));
			} else {
				System.out.println(String.format("Riik - %s: pealinn - %s; inglise keeles - %s; kohalikus keeles - %s ",
						citiesRow.get(3), citiesRow.get(1), citiesRow.get(0), citiesRow.get(2)));
			}
		}
		for (List<String> cityRow : citiesList) {
			cityRow.set(3, "country:".concat(cityRow.get(3)));
		}
		
		System.out.println("\t---ex.8---");
		PrintCities(citiesList);

		System.out.println("\t---ex.9---");
		citiesList.add(2,new ArrayList<>(Arrays.asList("Kurrunurruvutisaar", "LongStocking City", "Pikksuka linn", "Langstrump")));
		PrintCities(citiesList);
		
		System.out.println("\t---ex.10---");
		citiesList.remove(2);
		PrintCities(citiesList);
	}

}
